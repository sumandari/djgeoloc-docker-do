#!/bin/sh

ssh -o StrictHostKeyChecking=no root@$DIGITAL_OCEAN_IP_ADDRESS << 'ENDSSH'
  cd /app
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:web
  docker pull $IMAGE:db
  docker pull $IMAGE:nginx
  docker-compose -f docker-compose.prod.yml up -d
  docker-compose -f docker-compose.prod.yml exec -T web python manage.py migrate --noinput
  docker-compose -f docker-compose.prod.yml exec -T web python manage.py collectstatic --no-input --clear
ENDSSH