#!/bin/sh

echo DEBUG=0 >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
echo DJANGO_ALLOWED_HOSTS=$DIGITAL_OCEAN_IP_ADDRESS localhost 127.0.0.1 [::1] >> .env
echo SQL_ENGINE=django.contrib.gis.db.backends.postgis >> .env
echo DATABASE=postgres >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env

echo POSTGRES_DB=$SQL_DATABASE >> .env
echo POSTGRES_USER=$SQL_USER >> .env
echo POSTGRES_PASSWORD=$SQL_PASSWORD >> .env

echo WEB_IMAGE=$IMAGE:web  >> .env
echo DB_IMAGE=$IMAGE:db  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env

echo SQL_HOST=$IMAGE:db >> .env
echo SQL_PORT=5432 >> .env